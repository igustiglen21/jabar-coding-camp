// SOAL 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (var i=0; i<daftarHewan.length;i++){
  
  console.log(daftarHewan[i]);
}

// SOAL 2
* 
  function introduce(perkenalan) {
  var perkenalan = "Nama saya " + perkenalan.name +
                   ", umur saya " + perkenalan.age + 
                   " tahun, alamat saya di " + perkenalan.address + 
                   ", dan saya punya hobby yaitu " + perkenalan.hobby
  return perkenalan;
 }
*/
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// SOAL 3

var vokal = ["a", "i", "u", "e", "o"];

function hitung_huruf_vokal(kata) {
    var count = 0;
    for (var letter of kata.toLowerCase()) {
        if (vokal.includes(letter)) {
            count++;
        }
    }
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("iqbal");
console.log(hitung_1, hitung_2);

// SOAL 4

function hitung(y) {
	var hasil = y * 2 - 2;
  return hasil;
}
console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )