//SOAL 1 Function Penghasil Tanggal Hari Esok
//contoh 1
function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    var tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal, bulan, tahun);

//contoh 2
function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    var tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal, bulan, tahun);

//contoh 3
function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    var tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal, bulan, tahun);

//SOAL 2 Function Penghitung Jumlah Kata

function jumlah_kata(string) { 
  console.log (string.split(" ").length);
}

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
var kalimat_2 = "Saya Iqbal"
var kalimat_3 = "Saya Muhammad Iqbal Mubarok"

jumlah_kata(kalimat_1)
jumlah_kata(kalimat_2)
jumlah_kata(kalimat_3)
