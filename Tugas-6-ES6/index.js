//SOAL1

const luasPersegipanjang = function(p,l)
{
    return (p*l);
  return  2*(p+l);
};

const  luas = (p,l) => { return (p*l);};
const keliling = (p,l) => { return  2*(p+l);};
console.log('Luas =',luas(10,5));
console.log('Keliling =',keliling(10,5));

//SOAL 2

const newFunction = (firstName, lastName) => {
 return{
 firstName,
 lastName,
 fullName(){
 console.log(firstName + " " + lastName)
  }
 }
}
newFunction("William", "Imoh").fullName()

//SOAL 3

const newObject = {
 firstName: "Muhammad",
 lastName: "Iqbal Mubarok",
 address: "Jalan Ranamanyar",
 hobby : "playing football"
}

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby)

//SOAL 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

//SOAL 5

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet, consectetur adipiscing elit ${planet}`; 
console.log(before)