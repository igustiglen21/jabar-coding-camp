// Soal 1
var nilai = 75;

if (nilai >= 85)
{Indeks = "A" ;}

else if (nilai >= 75 && nilai < 85)
{Indeks = "B" ;}

else if (nilai >= 65 && nilai < 75)
{Indeks = "C" ;}

else if (nilai >= 55 && nilai < 65)
{Indeks = "D" ;}

else if (nilai < 55)
{Indeks ="E";}

console.log(Indeks);

// Soal 2
var tanggal = 21;
var bulan = 6;
var tahun = 2000;

var nama;

switch (bulan){
	case 1:
		nama = "Januari";
		break;
	case 2:
		nama = "Februari";
		break;
	case 3:
		nama = "Maret";
		break;
    case 4:
		nama = "April";
		break;
	case 5:
		nama = "Mei";
		break;
	case 6:
		nama = "Juni";
		break
    case 7:
		nama = "Juli";
		break;
	case 8:
		nama = "Agustus";
		break;
	case 9:
		nama = "September";
		break
    case 10:
		nama = "Oktober";
		break;
	case 11:
		nama = "November";
		break;
	case 12:
		nama = "Desember";
        break;
    default:
        nama = "Tidak Ada Bulan " + bulan;
}

var tg1 = tanggal.toString();
var th1 = tahun.toString();

console.log(tg1 + " " + nama + " " + th1);

// Soal 3

console.log("\n Output n=3");
var a = '';
var m = '#';
for(var i = 1; i <= 3; i++){
    for(var j = 0; j < i; j++)
        a += m;
    console.log(a);
    a = '';
}
console.log("\n Output n=7");
var a = '';
var m = '#';
for(var i = 1; i <= 7; i++){
    for(var j = 0; j < i; j++)
        a += m;
    console.log(a);
    a = '';
}

//Soal 4

var p = "Programming";
var j = "Javascript";
var v = "VueJS";
var nomor = 1;
var ulangan = 1;
var m = 10
while(ulangan <= m) {
    console.log(nomor+".- I Love "+p);
    nomor+=1;
    console.log(nomor+".- I Love "+j);
    nomor+=1;
    console.log(nomor+".- I Love "+v);
    console.log("=".repeat(nomor-1))
    nomor+=1;
    ulangan+=1;
}